﻿using DataModel.GenericRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel.UnitOfWork
{
    public interface IUnitOfWork
    {
        GenericRepository<MsProduct> ProductRepository { get; }
        GenericRepository<MsProductType> ProductTypeRepository { get; }
        GenericRepository<MsUser> UserRepository { get; }
        GenericRepository<TrCart> CartRepository { get; }
        GenericRepository<TrDiscount> DiscountRepository { get; }

        /// <summary>
        /// Save method.
        /// </summary>
        void Save();
    }
}
