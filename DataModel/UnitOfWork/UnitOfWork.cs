﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using DataModel.GenericRepository;

namespace DataModel.UnitOfWork
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        #region Private Member Variables

        private SALESTOCK_TAEntities _context = null;
        private GenericRepository<TrCart> _cartRepository;
        private GenericRepository<MsProduct> _productRepository;
        private GenericRepository<MsUser> _userRepository;
        private GenericRepository<TrDiscount> _discountRepository;
        private GenericRepository<MsProductType> _producttypeRepository;
        #endregion

        public UnitOfWork()
        {
            _context = new SALESTOCK_TAEntities();
        }

        #region Public Repository Creation Properties

        /// <summary>
        /// Get/Set Property for Product repository.
        /// </summary>
        public GenericRepository<MsProduct> ProductRepository
        {
            get
            {
                if (this._productRepository == null)
                    this._productRepository = new GenericRepository<MsProduct>(_context);
                return _productRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for User repository.
        /// </summary>
        public GenericRepository<MsUser> UserRepository
        {
            get
            {
                if (this._userRepository == null)
                    this._userRepository = new GenericRepository<MsUser>(_context);
                return _userRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for Cart repository.
        /// </summary>
        public GenericRepository<TrCart> CartRepository
        {
            get
            {
                if (this._cartRepository == null)
                    this._cartRepository = new GenericRepository<TrCart>(_context);
                return _cartRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for Discount repository.
        /// </summary>
        public GenericRepository<TrDiscount> DiscountRepository
        {
            get
            {
                if (this._discountRepository == null)
                    this._discountRepository = new GenericRepository<TrDiscount>(_context);
                return _discountRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for Product Type repository.
        /// </summary>
        public GenericRepository<MsProductType> ProductTypeRepository
        {
            get
            {
                if (this._producttypeRepository == null)
                    this._producttypeRepository = new GenericRepository<MsProductType>(_context);
                return _producttypeRepository;
            }
        }

        #endregion

        #region Public Member Methods
        /// <summary>
        /// Save method.
        /// </summary>
        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                var outputLines = new List<string>();
                foreach (var eve in e.EntityValidationErrors)
                {
                    outputLines.Add(string.Format(
                        "{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:", DateTime.Now,
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        outputLines.Add(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
                System.IO.File.AppendAllLines(@"C:\errorsSSCart.txt", outputLines);

                throw e;
            }

        }

        #endregion

        #region Implementing IDiosposable

        #region private dispose variable declaration
        private bool disposed = false;
        #endregion

        /// <summary>
        /// Protected Virtual Dispose method
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("UnitOfWork is being disposed");
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
