USE [SALESTOCK_TA]
GO
/****** Object:  Table [dbo].[MsProduct]    Script Date: 14/07/2016 06:42:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MsProduct](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductCode] [varchar](100) NULL,
	[ProductName] [varchar](100) NULL,
	[ProductType] [int] NULL,
	[ProductQty] [int] NULL,
	[UserUpdated] [varchar](100) NULL,
	[TimeUpdated] [datetime] NULL,
	[ProductPrice] [decimal](19, 4) NULL,
 CONSTRAINT [PK__MsProduc__B40CC6ED58A10AF9] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MsProductType]    Script Date: 14/07/2016 06:42:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MsProductType](
	[ProductTypeID] [int] IDENTITY(1,1) NOT NULL,
	[ProductTypeName] [varchar](100) NULL,
	[UserUpdated] [varchar](100) NULL,
	[TimeUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MsUser]    Script Date: 14/07/2016 06:42:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MsUser](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserCode] [varchar](100) NULL,
	[UserName] [varchar](100) NULL,
	[UserFullName] [varchar](100) NULL,
	[UserPassword] [varchar](100) NULL,
	[UserRole] [int] NULL,
	[UserUpdated] [varchar](100) NULL,
	[TimeUpdated] [datetime] NULL,
 CONSTRAINT [PK__MsUser__1788CCAC4B9BC4FF] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TrCart]    Script Date: 14/07/2016 06:42:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrCart](
	[CartID] [int] IDENTITY(1,1) NOT NULL,
	[CartCode] [varchar](100) NULL,
	[UserID] [int] NULL,
	[ProductID] [int] NULL,
	[ProductTypeID] [int] NULL,
	[ProductDiscount] [int] NULL,
	[ProductPrice] [decimal](19, 4) NULL,
	[ProductFinalPrice] [decimal](19, 4) NULL,
	[CartCreatedTime] [datetime] NULL,
	[CartExpiredTime] [datetime] NULL,
	[UserUpdated] [varchar](100) NULL,
	[TimeUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CartID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TrDiscount]    Script Date: 14/07/2016 06:42:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrDiscount](
	[DiscountID] [int] IDENTITY(1,1) NOT NULL,
	[DiscountCode] [varchar](100) NULL,
	[DiscountName] [varchar](100) NULL,
	[DiscountCoupon] [varchar](100) NULL,
	[DiscountType] [int] NULL,
	[DiscountPercentage] [int] NULL,
	[DiscountAmount] [decimal](19, 4) NULL,
	[ProductTypeID] [int] NULL,
	[DiscountMinPurchase] [decimal](19, 4) NULL,
	[DiscountMaxPurchase] [decimal](19, 4) NULL,
	[DiscountStartDate] [datetime] NULL,
	[DiscountEndDate] [datetime] NULL,
	[UserUpdated] [varchar](100) NULL,
	[TimeUpdated] [datetime] NULL,
 CONSTRAINT [PK__TrDiscou__E43F6DF69F615F62] PRIMARY KEY CLUSTERED 
(
	[DiscountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
