﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiddleTierServices
{
    public interface IComponent
    {
        void SetUp(IRegisterComponent registerComponent);
    }
}
