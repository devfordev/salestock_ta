# Sale Stock Cart #

This Solution is designed and developed to meet Sale Stock cart requirement, Product Cart Shopping add, remove item to cart, and also using coupon for discount case. 

### SSCart Solution is divided into Five (5) Main Layers, which consisted of ###

* DataModel
* Business Entities
* Business Services
* Middle Tier Services
* **SScart** - REST end points on this Solution

### Two Layers for Unit Testing Purpose, which consisted of ###

* Business Services Test
* Tester Helper

### Pattern and Framework used to achieve loosely coupled among SSCart Layers ###

* Unity Container
* Generic Repository
* Unit of Work

### Programming Languages used on SSCart Solution ###

* ASP.NET MVC 4 Web API C#
* Jquery

### Framework and Library used to build DataModel ###

* Entity Framework
* AutoMapper

### Routing Design ###

* using Attribute Routes

### Unit Testing ###

* NUnit
* Moq