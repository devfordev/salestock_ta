﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class ProductEntity
    {
        public int ProductID { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public Nullable<int> ProductType { get; set; }
        public Nullable<int> ProductQty { get; set; }
        public string UserUpdated { get; set; }
        public Nullable<System.DateTime> TimeUpdated { get; set; }
        public Nullable<decimal> ProductPrice { get; set; }
    }
    
}
