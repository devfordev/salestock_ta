﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class ProductTypeEntity
    {
        public int ProductTypeID { get; set; }
        public string ProductTypeName { get; set; }
        public string UserUpdated { get; set; }
        public Nullable<System.DateTime> TimeUpdated { get; set; }
    }
}
