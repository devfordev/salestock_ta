﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class DiscountEntity
    {
        public int DiscountID { get; set; }
        public string DiscountCode { get; set; }
        public string DiscountName { get; set; }
        public string DiscountCoupon { get; set; }
        public Nullable<int> DiscountType { get; set; }
        public Nullable<int> DiscountPercentage { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
        public Nullable<int> ProductTypeID { get; set; }
        public Nullable<decimal> DiscountMinPurchase { get; set; }
        public Nullable<decimal> DiscountMaxPurchase { get; set; }
        public Nullable<System.DateTime> DiscountStartDate { get; set; }
        public Nullable<System.DateTime> DiscountEndDate { get; set; }
        public string UserUpdated { get; set; }
        public Nullable<System.DateTime> TimeUpdated { get; set; }
    }
}
