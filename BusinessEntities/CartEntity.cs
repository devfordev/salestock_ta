﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class CartEntity
    {
        public int CartID { get; set; }
        public string CartCode { get; set; }
        public Nullable<int> UserID { get; set; }
        public Nullable<int> ProductID { get; set; }
        public Nullable<int> ProductTypeID { get; set; }
        public Nullable<int> ProductDiscount { get; set; }
        public Nullable<decimal> ProductPrice { get; set; }
        public Nullable<decimal> ProductFinalPrice { get; set; }
        public Nullable<System.DateTime> CartCreatedTime { get; set; }
        public Nullable<System.DateTime> CartExpiredTime { get; set; }
        public string UserUpdated { get; set; }
        public Nullable<System.DateTime> TimeUpdated { get; set; }
    }
}
