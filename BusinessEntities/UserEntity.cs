﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class UserEntity
    {
        public int UserID { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        public string UserPassword { get; set; }
        public Nullable<int> UserRole { get; set; }
        public string UserUpdated { get; set; }
        public Nullable<System.DateTime> TimeUpdated { get; set; }
    }
}
