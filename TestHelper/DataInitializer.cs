﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModel;

namespace TestHelper
{
    public class DataInitializer
    {
        public static List<MsProduct> GetAllProducts()
        {
            var products = new List<MsProduct>
            {
                new MsProduct() {ProductName = "ASUS X550JX"},
                new MsProduct() {ProductName = "ASUS ZENFONE 2"},
                new MsProduct() {ProductName = "ASUS ZENFONE 3"},
                new MsProduct() {ProductName = "VISUAL STUDIO 2016"},
                new MsProduct() {ProductName = "Microsoft Visio 2010"}
            };
            return products;
        }

        public static List<MsUser> GetAllUsers()
        {
            var users = new List<MsUser>
            {
                new MsUser()
                {
                    UserID = 10,
                    UserCode = "USRCD01",
                    UserName = "sampleusr1",
                },
                new MsUser()
                {
                    UserID = 11,
                    UserCode = "USRCD11",
                    UserName = "sampleusr11",
                },
                new MsUser()
                {
                    UserID = 12,
                    UserCode = "USRCD12",
                    UserName = "sampleusr12",
                }
            };

            return users;
        }
    }
}
