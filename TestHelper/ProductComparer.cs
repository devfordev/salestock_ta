﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModel;
using System.Collections;

namespace TestHelper
{
    public class ProductComparer : IComparer, IComparer<MsProduct>
    {
        public int Compare(object expected, object actual)
        {
            var lhs = expected as MsProduct;
            var rhs = actual as MsProduct;
            if (lhs == null || rhs == null) throw new InvalidOperationException();
            return Compare(lhs, rhs);
        }

        public int Compare(MsProduct expected, MsProduct actual)
        {
            int temp;
            return (temp = expected.ProductID.CompareTo(actual.ProductID)) != 0 ? temp : expected.ProductName.CompareTo(actual.ProductName);
        }
    }
}
