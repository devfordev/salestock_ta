﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;

namespace BusinessServices
{
    /// <summary>
    /// Product Service Contract
    /// </summary>
    public interface IProductServices
    {
        ProductEntity GetProductById(int ProductID);
        IEnumerable<ProductEntity> GetAllProducts();
        int CreateProduct(ProductEntity ProductEntity);
        bool UpdateProduct(int ProductID, ProductEntity ProductEntity);
        bool DeleteProduct(int ProductID);
    }
}
